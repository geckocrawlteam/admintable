import React from 'react';
import ItemDetail from './ItemDetail.jsx';

export default class AdminTable extends React.Component {

	render() {
		if (!this.props.item || !this.props.item._id)
			return (
				<div className="container">
					<div className="mt-4">
						<h1>{this.props.itemTitle}</h1>
						<p><i>({this.props.itemTitle} is currently not available)</i></p>
					</div>
				</div>

			);

		return (
			<ItemDetail
				fields={this.props.fields}
				item={this.props.item}
				itemTitle={this.props.itemTitle}
				updateFunction={this.props.updateFunction}
				mode="page"
			/>
		);
	}
}