import React, { Component } from 'react';
 
// Nav component
export default class Nav extends Component {
	constructor(props) {
		super(props);

		this.state={
			"current": props.current
		};
	}

	onLinkClick=(e)=> {
		e.preventDefault();

		var buttonId=e.target.dataset.key;

		if (this.props.onNavChange)
			this.props.onNavChange(buttonId);

		this.setState({
			"current": buttonId
		});
	}

	onTitleClick=(e)=>{
		e.preventDefault();

		if (this.props.env)
			window.open(Meteor.absoluteUrl({secure: true}));

		else if (this.props.onNavChange)
			this.props.onNavChange("");
	}

	renderTabs(tabs) {
		let els=[];

		for (let option in tabs) {
			if (typeof (tabs[option])=="object") {
				els.push(
					<div className="nav-item dropdown" key="dropdown">
						<a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{option}
						</a>
						<div className="dropdown-menu" aria-labelledby="navbarDropdown">
							{this.renderTabs(tabs[option])}
						</div>
					</div>
				);
			}

			else {
				let optionLabel=tabs[option];
				let classNames=["nav-item"];

				if (this.state.current==option)
					classNames.push("active");

				els.push(
					<li className={classNames.join(' ')} key={option}>
						<a className="nav-link" href="#" 
								data-key={option}
								onClick={this.onLinkClick}
						>{optionLabel}</a>
					</li>
				);
			}
		}

		return els;
	}

	render() {
		let leftOptions, rightOptions, menu;
		
		if (this.props.tabs && this.props.rightTabs){
			leftOptions=this.renderTabs(this.props.tabs);
			rightOptions=this.renderTabs(this.props.rightTabs);

			menu = [
				<button key="button" className="navbar-toggler" type="button" 
						data-toggle="collapse" data-target="#navbarColor01" 
						aria-controls="navbarColor01" aria-expanded="false" 
						aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>,
				<div key="dropdown" className="collapse navbar-collapse" id="navbarColor01">
					<ul className="navbar-nav mr-auto">
						{leftOptions}
					</ul>
					<ul className="navbar-nav ml-auto">
						{rightOptions}
						<li className="nav-item ">{this.props.avator}</li>
					</ul>
				</div>
			];
		} else {
			menu = this.props.avator;
		}

		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-primary">
				<div className="container">
					<a className="navbar-brand" href="#" onClick={this.onTitleClick}>{this.props.title}</a>
						{menu}
				</div>
			</nav>
		);
	}
}

