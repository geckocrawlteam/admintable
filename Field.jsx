import React from 'react';
import TextField from "./fields/TextField.jsx";

export default class Field {
	constructor(field){
		this.fieldDef=field;

		this.key = field.key;

		if (field.hasOwnProperty("label"))
			this.label = field.label;

		else
			this.label=this.key.slice(0,1).toUpperCase()+this.key.slice(1);

		this.type=field.type;
		if (!this.type)
			this.type=TextField;

		if (field.editable === false)
			this.editable = field.editable;

		else
			this.editable = true;

		this.fieldDef.editable=this.editable;
			
		if (field.condition)
			this.condition = field.condition;
		
		if (field.showInList === false)
			this.showInList = field.showInList;

		if (field.showInDetail === false)
			this.showInDetail = field.showInDetail;

		if (field.className)
			this.className=field.className;

		if (field.placeholder)
			this.placeholder = field.placeholder;
		
		if (field.textInputType)
			this.textInputType = field.textInputType;

		this.tab=field.tab;
	}

	createItemElement(item, mode, onChange) {
		let props={
			item: item,
			mode: mode,
			field: this.fieldDef,
			onChange: onChange
		};

		return React.createElement(this.type,props);
	}

	static create(field){
		return new Field(field);
	}
}

