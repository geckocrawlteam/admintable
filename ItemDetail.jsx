import React, { Component } from 'react'; 

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

export default class ItemDetail extends Component {
	constructor(props) {
		super(props);

		this.state={
			item: {},
			modified: false,
			working: false
		};
	}

	static getDerivedStateFromProps(props, state) {
		if (props.item)
			state.item=props.item;

		return state;
	}

	onDetailChange=(item)=>{
		this.setState({
			item: item,
			modified: true
		});
	} 

	onCloseClick=(e)=>{
		if (this.props.onClose)
			this.props.onClose();
	}

	save() {
		return new Promise((resolve, reject)=>{
			this.setState({
				working: true
			});

			//if (Meteor && typeof(Meteor.userId)=="function")
			//	this.state.item['owner'] = Meteor.userId();

			let callFunction=this.props.updateFunction;
			if (!this.state.item._id)
				callFunction=this.props.addFunction;

			Meteor.call(callFunction,this.state.item,(error, result)=>{
				this.setState({
					working: false
				});
				if (error) {
					reject(error);
				}

				else {
					if (!this.state.item._id && !result) {
						alert("Insert function did not return id!");
						reject("Insert function did not return id!");
					}

					else {
						if (!this.state.item._id) {
							let o=this.state.item;
							o._id=result;
							this.setState({
								item: o
							});
						}

						resolve();
					}
				}
			});
		});
	}

	onDetailSubmit=(e)=>{
		e.preventDefault();

		this.save().then(()=>{
			if (this.props.onClose)
				this.props.onClose();
		});
	}

	handleAction=(e)=>{
		let actionIndex=e.target.dataset.actionindex;
		let action=this.props.actions[actionIndex];

		var p=Promise.resolve();
		if (action.saveBeforeRun)
			p=this.save();

		p.then(()=>{
			this.setState({
				working: true
			});
			Meteor.call(action.actionFunction,this.state.item._id,(err,res)=>{
				if (err) {
					this.setState({
						working: false
					});
					alert(err.reason);
				}

				else {
					if (this.props.onClose)
						this.props.onClose();
				}
			});
		});
	}

	renderActions() {
		let dom=[];
		let actions=this.props.actions;
		if (!actions)
			actions=[];

		let actionIndex=0;
		for (let action of actions) {
			let disabled=false;
			if (this.state.working || 
					(this.state.modified && !action.saveBeforeRun))
				disabled=true;

			dom.push(
				<input type="button" value={action.name}
						className="btn btn-success"
						key={action.actionFunction}
						data-actionindex={actionIndex}
						onClick={this.handleAction}
						disabled={disabled}/>
			);

			actionIndex++;
		}

		return dom;
	}

	renderDeletebutton(){
		if (this.state.item._id && !this.state.working)
			return (
				<Button variant="danger" onClick={this.deleteItem}
						disabled={this.state.working}>
					Delete
				</Button>
			);
	}	

	renderSaveButton() {
		if (this.props.showSaveButton===false)
			return;

		return (
			<input type="submit" className="btn btn-primary" 
					disabled={this.state.working}
					value="Save"/>
		);
	}

	deleteItem=(e)=>{
		Meteor.call(this.props.deleteFunction, this.state.item._id);

		if (this.props.onClose)
			this.props.onClose();	
	}

	render() {
		let doTabs,tabs=[],renderedTabs=[];
		for (let field of this.props.fields) {
			if (tabs.indexOf(field.tab)<0)
				tabs.push(field.tab);
		}

		if (tabs.length>1 || tabs[0])
			doTabs=true;

		for (let tab of tabs) {
			let renderedFields=[];
			for (let field of this.props.fields) {
				var shouldRender=true;

				if (field.condition)
					shouldRender=field.condition(this.state.item);

				if (field.showInDetail===false)
					shouldRender=false;

				if (tab!=field.tab)
					shouldRender=false;

				if (shouldRender) {
					let content=field.createItemElement(this.state.item,"edit",this.onDetailChange);

					if (field.label)
						renderedFields.push(
							<Form.Group as={Row} key={field.key}>
								<Form.Label column sm={4}>{field.label}</Form.Label>
								<Col sm={8}>
									{content}
								</Col>
							</Form.Group>
						);

					else
						renderedFields.push(
							<Form.Group as={Row} key={field.key}>
								<Col sm={12}>
									{content}
								</Col>
							</Form.Group>
						);
				}
			}

			if (!tab)
				tab=this.props.itemTitle;

			if (doTabs)
				renderedTabs.push(
					<Tab title={tab} eventKey={tab} key={tab}>
						<fieldset className="mt-3">
							{renderedFields}
						</fieldset>
					</Tab>
				);

			else
				renderedTabs.push(
					<fieldset className="mt-3" key={tab}>
						{renderedFields}
					</fieldset>
				);
		}

		if (!tabs[0])
			tabs[0]=this.props.itemTitle;

		let title=this.state.item._id?"Edit ":"Add New ";
		title+=this.props.itemTitle;
		let submitValue=this.state.item._id?"Save":"Create";

		let tabContent;
		if (doTabs)
			tabContent=(
				<Tabs id="item-detail-tab" defaultActiveKey={tabs[0]}>
					{renderedTabs}
				</Tabs>
			);

		else
			tabContent=renderedTabs;

		switch (this.props.mode) {
			case "modal":
				return (
					<Modal size="lg" show onHide={this.onCloseClick} className={this.props.className}>
						<form onSubmit={this.onDetailSubmit}>
							<Modal.Header closeButton>
								<Modal.Title>{title}</Modal.Title>
							</Modal.Header>
							<Modal.Body>
								{tabContent}
							</Modal.Body>
							<Modal.Footer>
								{this.renderDeletebutton()}
								{this.renderActions()}
								{this.renderSaveButton()}
							</Modal.Footer>
						</form>
					</Modal>
				);
				break;

			case "page":
				return (
					<div className="container">
						<div className="mt-4">
							<form onSubmit={this.onDetailSubmit}>
								<h1>{this.props.itemTitle}</h1>
								{tabContent}
								{this.renderSaveButton()}
							</form>
						</div>
					</div>
				);
				break;
		}
	}
}
