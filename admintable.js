import "bootstrap";
import 'bootstrap/dist/css/bootstrap.css';

//import 'bootswatch/dist/minty/bootstrap.css';
//import 'bootswatch/dist/pulse/bootstrap.css';
//import 'bootswatch/dist/united/bootstrap.css';
import 'bootswatch/dist/superhero/bootstrap.css';

import Nav from "./Nav.jsx";
import AdminTable from "./AdminTable.jsx";
import Field from "./Field.jsx";

import TextField from "./fields/TextField.jsx";
import DropdownField from "./fields/DropdownField.jsx";
import AdminItem from "./AdminItem.jsx";
import ToggleSwitchField from "./fields/ToggleSwitchField";

const FieldType={
	TextField: TextField,
	DropdownField: DropdownField
};

export { 
	Nav,
	AdminTable,
	AdminItem,
	Field,

	FieldType,
	TextField,
	DropdownField,
	ToggleSwitchField
};
