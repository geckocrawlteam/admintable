import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import ItemTable from './ItemTable.jsx';
import ItemDetail from './ItemDetail.jsx';

import Alert from "react-bootstrap/Alert";

export default class AdminTable extends Component {
	constructor(props) {
		super(props);

		this.state={
			detailItem: null,
		}
	}

	static getDerivedStateFromProps(props, state) {
		let itemTitle=props.itemTitle;
		let itemPluralTitle=props.itemPluralTitle;
		let title=props.title;

		if (!itemTitle)
			itemTitle="Item";

		if (!itemPluralTitle)
			itemPluralTitle=itemTitle+"s";

		if (!title)
			title=itemPluralTitle;

		let newState={
			title: title,
			itemPluralTitle: itemPluralTitle,
			itemTitle: itemTitle
		}

		return newState;
	}

	createItem(item) {
		if (this.props.newItemFunc) {
			item=this.props.newItemFunc(item);

			Promise.resolve(item).then(
				(item)=>{
					this.setState({
						detailItem: item
					});
				},

				(err)=>{
					console.log("Can't create item: "+err);
				}
			);
		}

		else {
			this.setState({
				detailItem: item
			});
		}
	}

	onAddItemClick=(e)=>{
		e.preventDefault();
		this.createItem({});
	}

	onEditItemClick=(id)=> {
		let item=this.props.model.findOne({_id:id});

		if (this.props.openDetailFunc) {
			item=this.props.openDetailFunc(item);

			Promise.resolve(item).then(
				(item)=>{
					this.setState({
						detailItem: item
					});
				},

				(err)=>{
					console.log("Can't open item: "+err);
				}
			);
		}

		else {
			this.setState({
				detailItem: item
			});
		}
	}

	onDetailClose=(e)=>{
		this.setState({
			detailItem: null
		});
	}

	render() {
		let detail;
		if (this.state.detailItem) {
			detail=(
				<ItemDetail 
					collection={this.props.collection}
					model={this.props.model}
					fields={this.props.fields}
					onClose={this.onDetailClose}
					item={this.state.detailItem}
					itemTitle={this.state.itemTitle}
					key={this.state.detailItem._id}
					actions={this.props.actions}
					addFunction={this.props.addFunction}
					updateFunction={this.props.updateFunction}
					deleteFunction={this.props.deleteFunction}
					showSaveButton={this.props.showSaveButton}
					className={this.props.className}
					mode="modal"
				/>
			);
		}

		let classNames=["container"];
		if (this.props.className)
			classNames.push(this.props.className)

		return (
			<div className={classNames.join(" ")}>
				<div className="mt-4">
					<h1>{this.state.title}</h1>
					<a href="#" className="btn btn-primary" 
							onClick={this.onAddItemClick}>Add {this.state.itemTitle}</a>
					{detail}

					<ItemTable collection={this.props.collection}
						model={this.props.model}
						fields={this.props.fields}
						onItemClick={this.onEditItemClick}
					/>
				</div>
			</div>
		);
	}
}
