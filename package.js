Npm.depends({
  "bootstrap": '4.2.1',
  "bootswatch": '4.2.1',
  "popper.js": '1.14.6',
  "jquery": '3.3.1',
  "react-bootstrap": '1.0.0-beta.5'
});

Package.describe({
  name: 'admintable',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.8.0.2');
  api.use('ecmascript');

  api.add_files("admintable.css","client");

  api.mainModule('admintable.js','client');
});
