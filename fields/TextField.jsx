import React from "react";

export default class TextField extends React.Component {
	onChange=(e)=>{
		e.preventDefault();

		let item=this.props.item;
		item[this.props.field.key]=e.target.value;
		this.props.onChange(item);
	}

	render() {
		switch (this.props.mode) {
			case "list":
				if (!this.props.item[this.props.field.key])
					return null;

				return this.props.item[this.props.field.key];
				break;

			case "edit":
				let disabled=!this.props.field.editable;

				let v=this.props.item[this.props.field.key];
				if (!v)
					v="";

				return (
					<input type={this.props.field.textInputType ? this.props.field.textInputType : "text"} 
						className="form-control"
						value={v}
						onChange={this.onChange}
						disabled={disabled}
						placeholder={this.props.field.placeholder}
					/>
				);
				break;
		}
	}
}