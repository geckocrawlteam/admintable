import React, { Component } from "react";

export default class ToggleSwitchField extends Component {
	onChange=(e)=>{
		e.preventDefault();

		let item=this.props.item;
		
		item[this.props.field.key]=e.target.checked;
		this.props.onChange(item);
	}
	render(){
		let item=this.props.item;
		let checked = item[this.props.field.key] !== undefined ? item[this.props.field.key] : false;  

		if (checked==="false")
			checked=false;

		if (checked==="true")
			checked=true;

		switch (this.props.mode){
			case "edit":
				return (
					<div className="form-group">
						<div className="custom-control custom-switch">
						<input type="checkbox" className="custom-control-input" id={this.props.field.key}
							   value={this.props.item[this.props.field.key]}
							   onChange={this.onChange} checked={checked} />
						<label className="custom-control-label" htmlFor={this.props.field.key}>{this.props.field.label}</label>
						</div>
					</div>
				);
				break;
		}
	}
}