import React from "react";

export default class DropdownField extends React.Component {
	arrayElementById(array, id, idField) {
		if (!idField)
			idField="_id";

		if (!array)
			return null;

		for (el of array)
			if (el[idField]==id)
				return el;
	}

	onChange=(e)=>{
		e.preventDefault();

		let item=this.props.item;
		item[this.props.field.key]=e.target.value;
		this.props.onChange(item);
	}

	render() {
		switch (this.props.mode) {
			case "list":
				return this.renderList();
				break;

			case "edit":
				return this.renderEdit();
				break;
		}
	}

	renderList() {
		let val=this.props.item[this.props.field.key];

		if (this.props.field.dropdownItems
				&& this.props.field.dropdownItems[val])
			return this.props.field.dropdownItems[val];

		let foreignItem=this.arrayElementById(this.props.field.foreignCollection,
			val,this.props.field.foreignId);

		if (foreignItem)
			return foreignItem[this.props.field.foreignFieldLabel];

		return "-";
	}

	renderEdit() {
		let options;

		if (this.props.field.dropdownItems) {
			options=[];
			for (let option in this.props.field.dropdownItems) {
				options.push(
					<option value={option} key={option}>
						{this.props.field.dropdownItems[option]}
					</option>
				);
			}
		}

		else {
			let foreignId=this.props.field.foreignId;
			if (!foreignId)
				foreignId="_id";

			options=this.props.field.foreignCollection.map((collectionItem)=>{
				return (
					<option value={collectionItem[foreignId]}
							key={collectionItem[foreignId]}
							disabled={collectionItem[foreignId]===""}
						>
						{collectionItem[this.props.field.foreignFieldLabel]}
					</option>
				);
			});
		}
		let v = this.props.item[this.props.field.key];
		if (!v && typeof v !== "boolean")
			v = "";

		return (

			<select name={this.props.field.key} 
					onChange={this.onChange}
					value={v}
					className="custom-select">
				<option value="" key="-">-</option>
				<option value="" key="" disabled></option>
				{options}
			</select>
		);
	}
}