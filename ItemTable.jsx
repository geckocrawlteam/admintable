import React, { Component } from 'react';

export default class ItemTable extends Component {
	constructor(props){
		super(props);
	}
	onRowClick=(e)=>{
		e.preventDefault();
		let target=e.target;

		while (target && !target.dataset.id)
			target=target.parentNode;

		let id=target.dataset.id;
		if (this.props.onItemClick)
			this.props.onItemClick(id);
	}
	renderTh(fields){
		let headerFields = fields.map((field) => {
			return (
				<th key={field.key} className={field.className}>{field.label}</th>
			);
		});
		return (
			<thead className="thead-light">
				<tr>
					{headerFields}
				</tr>
			</thead>
		);	
	}

	renderTb(fields){
		let rowItems = this.props.collection.map((item) => {
			let row = [];
			let i = 0;
			for (let field of fields){
				field.mode = "list";
				let fieldVal = field.createItemElement(item,"list");
				if (i==0)
					row.push(<td key={field.key} className={field.className}>
						<a href="#" onClick={this.onRowClick} data-id={item._id}>{fieldVal}</a>
					</td>);

				else
					row.push(<td key={field.key} className={field.className}>{fieldVal}</td>);
			
				i++;
			}
			return (<tr key={item._id} className="item">{row}</tr>);
		});
		return rowItems;
	}
	render() {
		let fields = this.props.fields.slice();
		let useFields=[];
		if (this.props.collection.length > 0) 
			fields.forEach((field, i) => {
				if (field.showInList != false)
					useFields.push(field);
			});
		return (
			<table className="table mt-3 table-hover adminTable">
				{this.renderTh(useFields)}
				<tbody>
					{this.renderTb(useFields)}
				</tbody>
			</table>
		);
	}
}
